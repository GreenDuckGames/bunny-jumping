﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void EnemyAIElectro::Start()
extern void EnemyAIElectro_Start_m76DF2AB29C2B3150A191E69AF193C0A2861D6F2E (void);
// 0x00000002 System.Void EnemyAIElectro::Update()
extern void EnemyAIElectro_Update_m977D1E231A31EEB1A18E726B2874D414EF3AA3DE (void);
// 0x00000003 System.Void EnemyAIElectro::ToggleState()
extern void EnemyAIElectro_ToggleState_m889A09EB0764AE4B58D07069E044C749EC1BC548 (void);
// 0x00000004 System.Void EnemyAIElectro::ResetTimer()
extern void EnemyAIElectro_ResetTimer_mF0E2E2AB64A227D953A1ACD2B6F32782A85CCEF2 (void);
// 0x00000005 System.Collections.IEnumerator EnemyAIElectro::PreparingCoroutine()
extern void EnemyAIElectro_PreparingCoroutine_mD4448BAE9552718A6F8C9C4940916ED90FB3980C (void);
// 0x00000006 System.Void EnemyAIElectro::.ctor()
extern void EnemyAIElectro__ctor_mC9D2EB3E6F4CD5A687551AB451FE1FD7295AB7E6 (void);
// 0x00000007 System.Void EnemyAIElectro/<PreparingCoroutine>d__11::.ctor(System.Int32)
extern void U3CPreparingCoroutineU3Ed__11__ctor_m57B99C572A15CBC528DF4556409B0E4347AAD525 (void);
// 0x00000008 System.Void EnemyAIElectro/<PreparingCoroutine>d__11::System.IDisposable.Dispose()
extern void U3CPreparingCoroutineU3Ed__11_System_IDisposable_Dispose_m12192F874B8193890271E9FCDA1A0941D1F96D24 (void);
// 0x00000009 System.Boolean EnemyAIElectro/<PreparingCoroutine>d__11::MoveNext()
extern void U3CPreparingCoroutineU3Ed__11_MoveNext_m07DF5F7DA7E14AF3C93F0F0A397141D6C4965EB4 (void);
// 0x0000000A System.Object EnemyAIElectro/<PreparingCoroutine>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPreparingCoroutineU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE63A897624B88A1C2183F76AE2BC73AAE1B08089 (void);
// 0x0000000B System.Void EnemyAIElectro/<PreparingCoroutine>d__11::System.Collections.IEnumerator.Reset()
extern void U3CPreparingCoroutineU3Ed__11_System_Collections_IEnumerator_Reset_m90810D39D1DC2DD93E0909C2B5313F30924678A3 (void);
// 0x0000000C System.Object EnemyAIElectro/<PreparingCoroutine>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CPreparingCoroutineU3Ed__11_System_Collections_IEnumerator_get_Current_m85437F39D48B1C4B6930D90EAC745A9E2DED83E1 (void);
// 0x0000000D System.Void EnemyAISpike::Awake()
extern void EnemyAISpike_Awake_m537578279EDE0540B0D5839DBD5D39B191B4D7A3 (void);
// 0x0000000E System.Void EnemyAISpike::FixedUpdate()
extern void EnemyAISpike_FixedUpdate_m0FFE8A89EFEEBDB476C62DE1EC9FB2597AE5AF64 (void);
// 0x0000000F System.Void EnemyAISpike::.ctor()
extern void EnemyAISpike__ctor_mC345D9E926DB76C105F49819E4C498F2C44E99F5 (void);
// 0x00000010 System.Void KillableEnemy::Interact(UnityEngine.GameObject)
extern void KillableEnemy_Interact_m69A341A6DF401CE76681E6EA1315D06059B1D6B1 (void);
// 0x00000011 System.Void KillableEnemy::Die()
extern void KillableEnemy_Die_m771834B514B177AF4740DA5A79A91C44A7862E0D (void);
// 0x00000012 System.Void KillableEnemy::.ctor()
extern void KillableEnemy__ctor_mB882D8D04E8FF16E758A8C4BFB56C4F6ACCBFF79 (void);
// 0x00000013 System.Void AutoJump::Awake()
extern void AutoJump_Awake_mA231F992F3D21BCDFFB802EE889A4783E3F37E49 (void);
// 0x00000014 System.Void AutoJump::Update()
extern void AutoJump_Update_m2FAE88AED4711EEF602C24408787D66BDEBB4C07 (void);
// 0x00000015 System.Void AutoJump::Jump(System.Single)
extern void AutoJump_Jump_mD05ECC1EDEBE6F8EAE5FE938F7F334CB3D9B965A (void);
// 0x00000016 UnityEngine.Collider2D AutoJump::CheckGround()
extern void AutoJump_CheckGround_m0F8ED6E91C95E3D6DE3FE161DBE52B8FE7975DB9 (void);
// 0x00000017 System.Void AutoJump::InteractWithGround(UnityEngine.Collider2D)
extern void AutoJump_InteractWithGround_mAE2B544D27D06319EB2F2B357B3A0FEFD3BF376E (void);
// 0x00000018 System.Void AutoJump::.ctor()
extern void AutoJump__ctor_m88A2D088B28DD023F92A97D02F0FFE1522EC502B (void);
// 0x00000019 System.Void FollowObject::Start()
extern void FollowObject_Start_mA11E0F81D4C62E9582933C0A4F993FC28E82ED91 (void);
// 0x0000001A System.Void FollowObject::FixedUpdate()
extern void FollowObject_FixedUpdate_m7053AAACC61957760BBBE430804773BA27C01414 (void);
// 0x0000001B UnityEngine.Vector3 FollowObject::GetFollowingPosition(UnityEngine.Transform,UnityEngine.Vector3)
extern void FollowObject_GetFollowingPosition_m51B23CE59AFF72CFE501640DBBD8DB1FC486CED9 (void);
// 0x0000001C System.Void FollowObject::SetTargetObject(UnityEngine.Transform)
extern void FollowObject_SetTargetObject_m2160868B27C12E1D4D3A4F4C834B9EDC16F48FA8 (void);
// 0x0000001D System.Void FollowObject::Init()
extern void FollowObject_Init_m0183CCB052C8D82FF80B0864B3FFB33728ED0619 (void);
// 0x0000001E System.Void FollowObject::.ctor()
extern void FollowObject__ctor_mF626471207A22EA162AC1A6D8EC2EA23059B3C0E (void);
// 0x0000001F System.Void HurtObject::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void HurtObject_OnCollisionEnter2D_m1B1588F94E778F814A16C183ABA7933638159E6D (void);
// 0x00000020 System.Void HurtObject::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void HurtObject_OnTriggerEnter2D_m32CEFACEB741B955477906B17E70FFBBABDA3931 (void);
// 0x00000021 System.Void HurtObject::.ctor()
extern void HurtObject__ctor_mD0B86CB2FF914A0237CCF836ACEA7B996C74D9CD (void);
// 0x00000022 System.Void SideTeleport::Awake()
extern void SideTeleport_Awake_m7CF6ADE498E98436DC8E982ABE71C8765FE86679 (void);
// 0x00000023 System.Void SideTeleport::Update()
extern void SideTeleport_Update_m1584E981F5A0F47020FA73A8A77882FD60C74A39 (void);
// 0x00000024 System.Void SideTeleport::.ctor()
extern void SideTeleport__ctor_mF0FE03CFED179702ADF98F663F4F5C0BB537A8D2 (void);
// 0x00000025 System.Void IInteractable::Interact(UnityEngine.GameObject)
// 0x00000026 System.Void BrokenPlatform::Interact(UnityEngine.GameObject)
extern void BrokenPlatform_Interact_mB7DB78976683DF9E3EAF50467FEBFAF415CB806A (void);
// 0x00000027 System.Void BrokenPlatform::Break()
extern void BrokenPlatform_Break_mE9AA99D983452A6A844DA483924B34F1EC32B8AF (void);
// 0x00000028 System.Void BrokenPlatform::.ctor()
extern void BrokenPlatform__ctor_m23B73143C3F264B9C021781C46536A0C74073B8F (void);
// 0x00000029 System.Void MapGenerator::Awake()
extern void MapGenerator_Awake_m3FFBCFCBA88DF5C4CAF0C655CBA8AAABC49440D6 (void);
// 0x0000002A System.Void MapGenerator::Start()
extern void MapGenerator_Start_m9DFED1E1FAA34D894424666A17FAEFF41E0F9102 (void);
// 0x0000002B System.Void MapGenerator::Update()
extern void MapGenerator_Update_mCBFD98053CD9BA6C92FDB0E02362709E80AA235A (void);
// 0x0000002C System.Void MapGenerator::Generate()
extern void MapGenerator_Generate_m09D81605F363B5C89E7F01F9669C28B085B0629B (void);
// 0x0000002D System.Void MapGenerator::GeneratePlatforms()
extern void MapGenerator_GeneratePlatforms_m7C0F6117ABCD7641F8B2DF3933B3B583BF6CC1A1 (void);
// 0x0000002E System.Void MapGenerator::GenerateEnemies()
extern void MapGenerator_GenerateEnemies_m6E789D429A08B98C0714BA9692A38366EA946ED1 (void);
// 0x0000002F System.Void MapGenerator::.ctor()
extern void MapGenerator__ctor_mB9C58913280E7537C3EB6A1DACEDE418B41DFE01 (void);
// 0x00000030 System.Void PlatformCleaner::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void PlatformCleaner_OnTriggerEnter2D_mB7C8A594942ED1A2C78A58DCBD7766A5E74ECB37 (void);
// 0x00000031 System.Void PlatformCleaner::.ctor()
extern void PlatformCleaner__ctor_mB9B474406F63A9665F90A55E026FE916F26B9021 (void);
// 0x00000032 System.Void RandomEnviroment::Awake()
extern void RandomEnviroment_Awake_m6BC993B3D82D969CC9FC6DCD6216FD0592CCA2E0 (void);
// 0x00000033 System.Void RandomEnviroment::SetUpGreen()
extern void RandomEnviroment_SetUpGreen_m4A760861A2D2C8A2974B5E2891EB3EA093AA1519 (void);
// 0x00000034 System.Void RandomEnviroment::.ctor()
extern void RandomEnviroment__ctor_m21070D955D2D8F4D0773D7502C7E3D402BC459A9 (void);
// 0x00000035 System.Void Spring::Interact(UnityEngine.GameObject)
extern void Spring_Interact_m11BD381152CC9C5F0C4E8692FA12B5AA0FB3A719 (void);
// 0x00000036 System.Void Spring::.ctor()
extern void Spring__ctor_m7A1FF518446BAECE38938EA7B4D164A1361A58BB (void);
// 0x00000037 System.Void SpringSpawner::Awake()
extern void SpringSpawner_Awake_mCE660F3563332A53622107F7DCEDE14853153E37 (void);
// 0x00000038 System.Void SpringSpawner::SpawnSpring()
extern void SpringSpawner_SpawnSpring_mC9A831F99077E805F864E1D38588AFCF72DE2D4D (void);
// 0x00000039 System.Void SpringSpawner::.ctor()
extern void SpringSpawner__ctor_m8A888211FDF4412983BB90FB9A52BD8CAD9F12FE (void);
// 0x0000003A System.Void DestoroyAfterPlay::Start()
extern void DestoroyAfterPlay_Start_m3DFDAEFBCD76BF619FD360F245B607FC7A1CB44E (void);
// 0x0000003B System.Void DestoroyAfterPlay::.ctor()
extern void DestoroyAfterPlay__ctor_mBA9AEF29B5011D1DF34FF5082AF9187AACDFA852 (void);
// 0x0000003C System.Void Movement::Awake()
extern void Movement_Awake_mC24012D10E5FE2E1CF2B188AA4EFC347C0DA7A93 (void);
// 0x0000003D System.Void Movement::FixedUpdate()
extern void Movement_FixedUpdate_mACA59B810F6DAD27F591A83046A0643AADBAF1EB (void);
// 0x0000003E System.Void Movement::.ctor()
extern void Movement__ctor_mEA4800F5BE98787C0ACA8CDF85918B56DE62A2AB (void);
// 0x0000003F System.Void Player::GameOver()
extern void Player_GameOver_m2484AE1CBC60A0A4AE50E58E0529BBD0B44FFB47 (void);
// 0x00000040 System.Void Player::OnDestroy()
extern void Player_OnDestroy_m2A962F7BBA90A997ED723C07BF86A0681AA24F6E (void);
// 0x00000041 System.Void Player::.ctor()
extern void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (void);
// 0x00000042 System.Void PlayerRotater::Awake()
extern void PlayerRotater_Awake_mB437688A8AC038E0D1ECCBB25CCB067D6DD99D83 (void);
// 0x00000043 System.Void PlayerRotater::FixedUpdate()
extern void PlayerRotater_FixedUpdate_m664AF6991A02252224A86B731DA0FA400E2096C7 (void);
// 0x00000044 System.Void PlayerRotater::.ctor()
extern void PlayerRotater__ctor_mD89277A2B351B38CD4BBC2CAEF251643833C2658 (void);
// 0x00000045 System.Int32 ScoreCounter::get_Score()
extern void ScoreCounter_get_Score_m8F399F02755E7BA5BE67E782B9EF8A715B6663FF (void);
// 0x00000046 System.Void ScoreCounter::set_Score(System.Int32)
extern void ScoreCounter_set_Score_mEABEC6EDE7F91AF7FD3D1DB14384B01B58B4F153 (void);
// 0x00000047 System.Void ScoreCounter::Awake()
extern void ScoreCounter_Awake_m0C836AB8997419FD5FB473D337379FB2ED1D7B65 (void);
// 0x00000048 System.Void ScoreCounter::Start()
extern void ScoreCounter_Start_m7CA1B3AD27116C1CECC5C1FDB17949555C71466A (void);
// 0x00000049 System.Void ScoreCounter::Update()
extern void ScoreCounter_Update_mF26D62754A1473CC6A65C248976289163B0B80F4 (void);
// 0x0000004A System.Void ScoreCounter::.ctor()
extern void ScoreCounter__ctor_mB45D52B20E0B074E89863294E5D0DC9624017C8B (void);
// 0x0000004B UnityEngine.GameObject ObjectSet::GetRandomObject()
extern void ObjectSet_GetRandomObject_m4BC0249E8303851179F2761A025621F3B7D2260B (void);
// 0x0000004C System.Void ObjectSet::.ctor()
extern void ObjectSet__ctor_m0FBFA96423A1AE7BF6D23ABFB523458B8EE0BCDD (void);
// 0x0000004D UnityEngine.Sprite SpriteSet::GetRandomSprite()
extern void SpriteSet_GetRandomSprite_m9C80BAD92AD08F5C3FB5F6E3D8F2A076B80F2C69 (void);
// 0x0000004E System.Void SpriteSet::.ctor()
extern void SpriteSet__ctor_mECB4FE2A234ACD29F1B85F6C25199C1A0D02702A (void);
// 0x0000004F System.Void BlinkHint::Start()
extern void BlinkHint_Start_mD7862DF666AF8AFDE1B285DBA122EE47470E8BEA (void);
// 0x00000050 System.Collections.IEnumerator BlinkHint::Blink()
extern void BlinkHint_Blink_m2C909D41866D90BA35D3F9A5DD421F10E8CDA483 (void);
// 0x00000051 System.Void BlinkHint::.ctor()
extern void BlinkHint__ctor_m7DF50E8B12DEF402CBC70194626DD253ED004ADF (void);
// 0x00000052 System.Void BlinkHint/<Blink>d__5::.ctor(System.Int32)
extern void U3CBlinkU3Ed__5__ctor_mBD162307CFF6C1638BBE1EBE823DBE9FBB2B6E39 (void);
// 0x00000053 System.Void BlinkHint/<Blink>d__5::System.IDisposable.Dispose()
extern void U3CBlinkU3Ed__5_System_IDisposable_Dispose_m25F03F67A2AAD8511BDD445F76B834606B57E105 (void);
// 0x00000054 System.Boolean BlinkHint/<Blink>d__5::MoveNext()
extern void U3CBlinkU3Ed__5_MoveNext_mEC893D33D5BD3E7F81CB09B07923DBF7F735D38F (void);
// 0x00000055 System.Object BlinkHint/<Blink>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBlinkU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5CD165FBDFFE956822A616517CB577BDC669E621 (void);
// 0x00000056 System.Void BlinkHint/<Blink>d__5::System.Collections.IEnumerator.Reset()
extern void U3CBlinkU3Ed__5_System_Collections_IEnumerator_Reset_m16670475C85B2D3B5FFE5204FEE010773E8CB255 (void);
// 0x00000057 System.Object BlinkHint/<Blink>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CBlinkU3Ed__5_System_Collections_IEnumerator_get_Current_mDF514C511338BB14788E6FD38A908B2FF0CF4044 (void);
// 0x00000058 System.Void FirstMovement::Update()
extern void FirstMovement_Update_mAF2FDCEF3D868D6443BA1172E968600405183DFC (void);
// 0x00000059 System.Void FirstMovement::.ctor()
extern void FirstMovement__ctor_m1AF9B9384863BA156CCB7DACD06E276BA0E3020B (void);
// 0x0000005A System.Void ScoreLable::Awake()
extern void ScoreLable_Awake_m0160F0050E248F02C964E707E20148D847556DD2 (void);
// 0x0000005B System.Void ScoreLable::Update()
extern void ScoreLable_Update_m045D222CF7BA421D45FED91CB3011367EE15C448 (void);
// 0x0000005C System.Void ScoreLable::.ctor()
extern void ScoreLable__ctor_mF4DBD0EE0768254A6BCC1EFBBF6F020D98D4997C (void);
static Il2CppMethodPointer s_methodPointers[92] = 
{
	EnemyAIElectro_Start_m76DF2AB29C2B3150A191E69AF193C0A2861D6F2E,
	EnemyAIElectro_Update_m977D1E231A31EEB1A18E726B2874D414EF3AA3DE,
	EnemyAIElectro_ToggleState_m889A09EB0764AE4B58D07069E044C749EC1BC548,
	EnemyAIElectro_ResetTimer_mF0E2E2AB64A227D953A1ACD2B6F32782A85CCEF2,
	EnemyAIElectro_PreparingCoroutine_mD4448BAE9552718A6F8C9C4940916ED90FB3980C,
	EnemyAIElectro__ctor_mC9D2EB3E6F4CD5A687551AB451FE1FD7295AB7E6,
	U3CPreparingCoroutineU3Ed__11__ctor_m57B99C572A15CBC528DF4556409B0E4347AAD525,
	U3CPreparingCoroutineU3Ed__11_System_IDisposable_Dispose_m12192F874B8193890271E9FCDA1A0941D1F96D24,
	U3CPreparingCoroutineU3Ed__11_MoveNext_m07DF5F7DA7E14AF3C93F0F0A397141D6C4965EB4,
	U3CPreparingCoroutineU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE63A897624B88A1C2183F76AE2BC73AAE1B08089,
	U3CPreparingCoroutineU3Ed__11_System_Collections_IEnumerator_Reset_m90810D39D1DC2DD93E0909C2B5313F30924678A3,
	U3CPreparingCoroutineU3Ed__11_System_Collections_IEnumerator_get_Current_m85437F39D48B1C4B6930D90EAC745A9E2DED83E1,
	EnemyAISpike_Awake_m537578279EDE0540B0D5839DBD5D39B191B4D7A3,
	EnemyAISpike_FixedUpdate_m0FFE8A89EFEEBDB476C62DE1EC9FB2597AE5AF64,
	EnemyAISpike__ctor_mC345D9E926DB76C105F49819E4C498F2C44E99F5,
	KillableEnemy_Interact_m69A341A6DF401CE76681E6EA1315D06059B1D6B1,
	KillableEnemy_Die_m771834B514B177AF4740DA5A79A91C44A7862E0D,
	KillableEnemy__ctor_mB882D8D04E8FF16E758A8C4BFB56C4F6ACCBFF79,
	AutoJump_Awake_mA231F992F3D21BCDFFB802EE889A4783E3F37E49,
	AutoJump_Update_m2FAE88AED4711EEF602C24408787D66BDEBB4C07,
	AutoJump_Jump_mD05ECC1EDEBE6F8EAE5FE938F7F334CB3D9B965A,
	AutoJump_CheckGround_m0F8ED6E91C95E3D6DE3FE161DBE52B8FE7975DB9,
	AutoJump_InteractWithGround_mAE2B544D27D06319EB2F2B357B3A0FEFD3BF376E,
	AutoJump__ctor_m88A2D088B28DD023F92A97D02F0FFE1522EC502B,
	FollowObject_Start_mA11E0F81D4C62E9582933C0A4F993FC28E82ED91,
	FollowObject_FixedUpdate_m7053AAACC61957760BBBE430804773BA27C01414,
	FollowObject_GetFollowingPosition_m51B23CE59AFF72CFE501640DBBD8DB1FC486CED9,
	FollowObject_SetTargetObject_m2160868B27C12E1D4D3A4F4C834B9EDC16F48FA8,
	FollowObject_Init_m0183CCB052C8D82FF80B0864B3FFB33728ED0619,
	FollowObject__ctor_mF626471207A22EA162AC1A6D8EC2EA23059B3C0E,
	HurtObject_OnCollisionEnter2D_m1B1588F94E778F814A16C183ABA7933638159E6D,
	HurtObject_OnTriggerEnter2D_m32CEFACEB741B955477906B17E70FFBBABDA3931,
	HurtObject__ctor_mD0B86CB2FF914A0237CCF836ACEA7B996C74D9CD,
	SideTeleport_Awake_m7CF6ADE498E98436DC8E982ABE71C8765FE86679,
	SideTeleport_Update_m1584E981F5A0F47020FA73A8A77882FD60C74A39,
	SideTeleport__ctor_mF0FE03CFED179702ADF98F663F4F5C0BB537A8D2,
	NULL,
	BrokenPlatform_Interact_mB7DB78976683DF9E3EAF50467FEBFAF415CB806A,
	BrokenPlatform_Break_mE9AA99D983452A6A844DA483924B34F1EC32B8AF,
	BrokenPlatform__ctor_m23B73143C3F264B9C021781C46536A0C74073B8F,
	MapGenerator_Awake_m3FFBCFCBA88DF5C4CAF0C655CBA8AAABC49440D6,
	MapGenerator_Start_m9DFED1E1FAA34D894424666A17FAEFF41E0F9102,
	MapGenerator_Update_mCBFD98053CD9BA6C92FDB0E02362709E80AA235A,
	MapGenerator_Generate_m09D81605F363B5C89E7F01F9669C28B085B0629B,
	MapGenerator_GeneratePlatforms_m7C0F6117ABCD7641F8B2DF3933B3B583BF6CC1A1,
	MapGenerator_GenerateEnemies_m6E789D429A08B98C0714BA9692A38366EA946ED1,
	MapGenerator__ctor_mB9C58913280E7537C3EB6A1DACEDE418B41DFE01,
	PlatformCleaner_OnTriggerEnter2D_mB7C8A594942ED1A2C78A58DCBD7766A5E74ECB37,
	PlatformCleaner__ctor_mB9B474406F63A9665F90A55E026FE916F26B9021,
	RandomEnviroment_Awake_m6BC993B3D82D969CC9FC6DCD6216FD0592CCA2E0,
	RandomEnviroment_SetUpGreen_m4A760861A2D2C8A2974B5E2891EB3EA093AA1519,
	RandomEnviroment__ctor_m21070D955D2D8F4D0773D7502C7E3D402BC459A9,
	Spring_Interact_m11BD381152CC9C5F0C4E8692FA12B5AA0FB3A719,
	Spring__ctor_m7A1FF518446BAECE38938EA7B4D164A1361A58BB,
	SpringSpawner_Awake_mCE660F3563332A53622107F7DCEDE14853153E37,
	SpringSpawner_SpawnSpring_mC9A831F99077E805F864E1D38588AFCF72DE2D4D,
	SpringSpawner__ctor_m8A888211FDF4412983BB90FB9A52BD8CAD9F12FE,
	DestoroyAfterPlay_Start_m3DFDAEFBCD76BF619FD360F245B607FC7A1CB44E,
	DestoroyAfterPlay__ctor_mBA9AEF29B5011D1DF34FF5082AF9187AACDFA852,
	Movement_Awake_mC24012D10E5FE2E1CF2B188AA4EFC347C0DA7A93,
	Movement_FixedUpdate_mACA59B810F6DAD27F591A83046A0643AADBAF1EB,
	Movement__ctor_mEA4800F5BE98787C0ACA8CDF85918B56DE62A2AB,
	Player_GameOver_m2484AE1CBC60A0A4AE50E58E0529BBD0B44FFB47,
	Player_OnDestroy_m2A962F7BBA90A997ED723C07BF86A0681AA24F6E,
	Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7,
	PlayerRotater_Awake_mB437688A8AC038E0D1ECCBB25CCB067D6DD99D83,
	PlayerRotater_FixedUpdate_m664AF6991A02252224A86B731DA0FA400E2096C7,
	PlayerRotater__ctor_mD89277A2B351B38CD4BBC2CAEF251643833C2658,
	ScoreCounter_get_Score_m8F399F02755E7BA5BE67E782B9EF8A715B6663FF,
	ScoreCounter_set_Score_mEABEC6EDE7F91AF7FD3D1DB14384B01B58B4F153,
	ScoreCounter_Awake_m0C836AB8997419FD5FB473D337379FB2ED1D7B65,
	ScoreCounter_Start_m7CA1B3AD27116C1CECC5C1FDB17949555C71466A,
	ScoreCounter_Update_mF26D62754A1473CC6A65C248976289163B0B80F4,
	ScoreCounter__ctor_mB45D52B20E0B074E89863294E5D0DC9624017C8B,
	ObjectSet_GetRandomObject_m4BC0249E8303851179F2761A025621F3B7D2260B,
	ObjectSet__ctor_m0FBFA96423A1AE7BF6D23ABFB523458B8EE0BCDD,
	SpriteSet_GetRandomSprite_m9C80BAD92AD08F5C3FB5F6E3D8F2A076B80F2C69,
	SpriteSet__ctor_mECB4FE2A234ACD29F1B85F6C25199C1A0D02702A,
	BlinkHint_Start_mD7862DF666AF8AFDE1B285DBA122EE47470E8BEA,
	BlinkHint_Blink_m2C909D41866D90BA35D3F9A5DD421F10E8CDA483,
	BlinkHint__ctor_m7DF50E8B12DEF402CBC70194626DD253ED004ADF,
	U3CBlinkU3Ed__5__ctor_mBD162307CFF6C1638BBE1EBE823DBE9FBB2B6E39,
	U3CBlinkU3Ed__5_System_IDisposable_Dispose_m25F03F67A2AAD8511BDD445F76B834606B57E105,
	U3CBlinkU3Ed__5_MoveNext_mEC893D33D5BD3E7F81CB09B07923DBF7F735D38F,
	U3CBlinkU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5CD165FBDFFE956822A616517CB577BDC669E621,
	U3CBlinkU3Ed__5_System_Collections_IEnumerator_Reset_m16670475C85B2D3B5FFE5204FEE010773E8CB255,
	U3CBlinkU3Ed__5_System_Collections_IEnumerator_get_Current_mDF514C511338BB14788E6FD38A908B2FF0CF4044,
	FirstMovement_Update_mAF2FDCEF3D868D6443BA1172E968600405183DFC,
	FirstMovement__ctor_m1AF9B9384863BA156CCB7DACD06E276BA0E3020B,
	ScoreLable_Awake_m0160F0050E248F02C964E707E20148D847556DD2,
	ScoreLable_Update_m045D222CF7BA421D45FED91CB3011367EE15C448,
	ScoreLable__ctor_mF4DBD0EE0768254A6BCC1EFBBF6F020D98D4997C,
};
static const int32_t s_InvokerIndices[92] = 
{
	1515,
	1515,
	1515,
	1515,
	1473,
	1515,
	1257,
	1515,
	1493,
	1473,
	1515,
	1473,
	1515,
	1515,
	1515,
	1267,
	1515,
	1515,
	1515,
	1515,
	1286,
	1473,
	1267,
	1515,
	1515,
	1515,
	629,
	1267,
	1515,
	1515,
	1267,
	1267,
	1515,
	1515,
	1515,
	1515,
	1267,
	1267,
	1515,
	1515,
	1515,
	1515,
	1515,
	1515,
	1515,
	1515,
	1515,
	1267,
	1515,
	1515,
	1515,
	1515,
	1267,
	1515,
	1515,
	1515,
	1515,
	1515,
	1515,
	1515,
	1515,
	1515,
	1515,
	1515,
	1515,
	1515,
	1515,
	1515,
	1461,
	1257,
	1515,
	1515,
	1515,
	1515,
	1473,
	1515,
	1473,
	1515,
	1515,
	1473,
	1515,
	1257,
	1515,
	1493,
	1473,
	1515,
	1473,
	1515,
	1515,
	1515,
	1515,
	1515,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	92,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
